halide (19.0.0-5) unstable; urgency=medium

  * Make other parts of `mullapudi2016_reorder` test even more lax to timings.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Tue, 21 Jan 2025 20:03:28 +0300

halide (19.0.0-4) unstable; urgency=medium

  * Unbreak the build - should be built *WITH* assertions...
    See https://github.com/halide/Halide/issues/8550.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Mon, 20 Jan 2025 21:57:09 +0300

halide (19.0.0-3) unstable; urgency=medium

  * package's i386 build i s failing with OOM related to debug info,
    so rip out DWZ crutches, enable DWARF5, split+compressed debug info.
    Decreases total amd64 dbgsym size 32Mb -> 4.8Mb, and makes build faster.
    This either solves it, or i386 build needs to be disabled.
  * Respect specified job count when running CTest.
  * Install few more doc files that were missed.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 08 Jan 2025 23:58:22 +0300

halide (19.0.0-2) unstable; urgency=medium

  * Make package buildable on i386 again.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Mon, 23 Dec 2024 02:09:40 +0300

halide (19.0.0-1) unstable; urgency=medium

  * Update to latest upstream major release
  * Build with/against LLVM19
  * Refresh and reorder patches, drop patches applied upstream
  * Stop specifying Release CMake Build Mode, this is required
    for `find_package(flatbuffers)` to work,
    since it only provides `None` mode.
  * Looks like WebAssembly target can no longer be turned off,
    so `liblld` is now a dependency.
  * `libhalide-dev` no longer depends on python,
    `python3-halide19-dev` has been split off.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 18 Dec 2024 00:26:37 +0300

halide (18.0.0-8) unstable; urgency=medium

  * No changes, source-only upload to allow testing migration.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 28 Aug 2024 08:30:25 +0300

halide (18.0.0-7) unstable; urgency=medium

  * Build against LLVM18.
  * Fix and re-enable Python bindings on i386.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sat, 10 Aug 2024 22:51:29 +0300

halide (18.0.0-6) unstable; urgency=medium

  * When building arch:all packages only (i.e. `binary-indep` make target),
    disable LTO/PGO/tests, since they are completely pointless for them.
  * Run basic integration test with both the clang and gcc.
  * Fix autopkgtest failures on i386 with gcc.
  * Add `nopython` build profile: `python3-halide-doc` binary package
    is arch:all, but can't be built on architectures on which we can't build
    python bindings, and it does not seem possible to reflect that in `control`.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 07 Aug 2024 21:59:09 +0300

halide (18.0.0-5) unstable; urgency=medium

  * `mullapudi2016_reorder` "performance test" is known to be problematic, and
    upstream was already workarounding it's timing-related flakiness.
    Make that test even more lax.
  * Don't depend on `python3-halide` from `libhalide18-dev` on i386,
    where former does not exist.
  * Actual debian autopkgtest runner CI comes without `build-essential`
    pre-installed, so add it to deps of the `00-test-integration` test.
  * Disable `01-test-python-bindings` autopkgtest on i386,
    where `python3-halide` does not exist.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sun, 04 Aug 2024 06:52:49 +0300

halide (18.0.0-4) unstable; urgency=medium

  * Improve autopkgtest: actually ensure that deps are listed as such.
  * Repair i386 package build: `libHalidePyStubs.a` does not exist there.
  * Collect PGO counters atomically. Does not really help
    with reproducibility of the builds though.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sat, 03 Aug 2024 05:48:53 +0300

halide (18.0.0-3) unstable; urgency=medium

  * `python3-halide` is not `MA:same`/co-installable, don't mark it as such.
  * Add missing dependency from `libhalide18-dev` on `python3-halide`,
    if latter is not present CMake's `find_package(Halide)` fails.
  * Thus `libhalide-dev` also becomes non `MA:same`/co-installable...
  * Build `Halide::PyStub` library without LTO, so that after install
    it can actually be linked against.
  * The previous point somehow makes `strip` unhappy about full 3-stage
    (but not PGO-less!) `libHalidePyStubs.a`, so use `llvm-strip` instead.
  * Finally, add some autopkgtests!
  * Update standards-version to 4.7.0, no changes needed.
  * Reorder patches.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 31 Jul 2024 20:03:21 +0300

halide (18.0.0-2) unstable; urgency=medium

  * Re-silence failures of flaky tests (Closes: #1076968)

 -- Roman Lebedev <lebedev.ri@gmail.com>  Thu, 25 Jul 2024 06:55:28 +0300

halide (18.0.0-1) unstable; urgency=medium

  * Update to latest upstream release.
  * Drop patches that have been applied upstream.
  * Still build with/against LLVM17.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Fri, 19 Jul 2024 01:11:11 +0300

halide (17.0.2-2) unstable; urgency=medium

  * Fully go back to building using LLVM17,
    since build-dep on LLVM18 is, obviously,
    also preventing migration to testing.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sun, 30 Jun 2024 03:17:44 +0300

halide (17.0.2-1) unstable; urgency=medium

  * Update to newest upstream point release
  * Go back to building against LLVM17, resulting in a soversion bump,
    see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1067005
  * Drop soversion from -dev package name,
    only one can be installed at the same time,
    add necessary breaks+replaces (Closes: #1066057)
  * Add explicit timeout for tests, allow them to be rerun at least once,
    to avoid spurious build failures due to flaky CI infra (Closes: #1069403)
  * Allow all tests to be rerun at least once, to avoid spurious build failures
    due to flaky CI infra (Closes: #1068138)
  * 64-bit `time_t` transition rebuild

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 26 Jun 2024 04:33:35 +0300

halide (17.0.1-1) unstable; urgency=medium

  * Update to newest upstream point release
  * Build against LLVM18.
  * Re-disable RISCV64 build, still not supported upstream.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sat, 09 Mar 2024 00:27:53 +0300

halide (17.0.0-2) unstable; urgency=medium

  * Enable RISCV64 build
  * Don't build-depend on atlas. Fixes #1056675.
  * Do provide `override_dh_clean` target. Fixes #1044671.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Fri, 16 Feb 2024 23:26:19 +0300

halide (17.0.0-1) unstable; urgency=medium

  * New upstream major release.
  * Build against LLVM17.

  [ Sylvestre ]
  * Update standards version to 4.6.2, no changes needed.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Fri, 02 Feb 2024 03:34:49 +0300

halide (16.0.0-3) unstable; urgency=medium

  * Explicitly disable python bindings on i386, unsupported upstream,
    used to work by chance in earlier halide versions.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 12 Jul 2023 19:56:06 +0300

halide (16.0.0-2) unstable; urgency=medium

  * Ignore failures of performance tests.
  * Correctly detect `_Float16` as not supported on i386

 -- Roman Lebedev <lebedev.ri@gmail.com>  Tue, 11 Jul 2023 22:39:59 +0300

halide (16.0.0-1) unstable; urgency=medium

  * New upstream major release.
  * Build against LLVM16.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Thu, 06 Jul 2023 21:04:39 +0300

halide (14.0.0-3) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team upload

  [ James Addison ]
  * Add libclang-rt-14-dev build dependency (Closes: #1027939)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 23 Jan 2023 13:19:16 +0100

halide (14.0.0-2) unstable; urgency=medium

  * Enable second PGO round (call-site-aware)
  * Use -Wl,--build-id=sha1 to make sure debugedit can do its job.
    Cherry-picked from Gianfranco Costamagna's change for Ubuntu.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Wed, 20 Apr 2022 15:00:55 +0300

halide (14.0.0-1) unstable; urgency=medium

  * New upstream major release.
  * Build against LLVM14.
  * Effectively disable flaky performance_boundary_conditions test.
  * Strip unusual field spacing from debian/control.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Fri, 08 Apr 2022 12:51:46 +0300

halide (13.0.4-1) unstable; urgency=medium

  * Update to latest upstream release.
  * Fine-tune some more missing dependencies.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sat, 22 Jan 2022 14:47:58 +0300

halide (13.0.3-2) unstable; urgency=medium

  * Declare missing Breaks+Replaces on halide13-api-doc. Fixes #1003424.
  * Drop liblld dependency - only needed for WebAssembly,
    but we don't build that anyway.
  * Don't enable LLD/PGO on some more architectures where that fails.
  * Fine-tune some more missing dependencies.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Mon, 10 Jan 2022 12:15:10 +0300

halide (13.0.3-1) unstable; urgency=medium

  * Update to latest upstream release, refresh patches.
  * Package Python3 bindings.
    Thanks goes to Mark Glines for bringing this up initially.
  * Split Doxygen API docs into their own package, separate from C++ tutorials.
  * Package Python3 tutorials.
  * Yet another RISC-V build fix.
  * Don't hard-require libomp, it isn't strictly needed.
  * Explicitly overrule DEB_BUILD_MAINT_OPTIONS's request to do LTO,
    since we do our own LTO handling. Might fix Ubuntu build.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Thu, 06 Jan 2022 15:14:53 +0300

halide (13.0.2-2) unstable; urgency=medium

  * Another RISC-V build fix
  * Instruct CTest to rerun failed tests a few times to weed out
    intermitten test failures
  * Rebuild to pacify intermitten test failures

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sun, 19 Dec 2021 13:33:16 +0300

halide (13.0.2-1) unstable; urgency=medium

  * Update to latest upstream release, drop upstreamed patches.
  * Another attempt at fixing RISC-V / etc LLD-related build failures.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Fri, 10 Dec 2021 22:02:48 +0300

halide (13.0.1-4) unstable; urgency=medium

  [ Sylvestre Ledru ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Roman Lebedev ]
  * Hopefully fix i386 build - add missing preprocessor check for SSE.
  * Hopefully fix riscv64 build - disable PGO/LTO.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Mon, 06 Dec 2021 23:34:00 +0300

halide (13.0.1-3) unstable; urgency=medium

  * Use LLVM LLD linker
  * Also build halide test apps
  * Perform 2-stage build (PGO)
  * Mark `libhalide13-doc` as `Multi-Arch: foreign`, as
    suggested by multiarch hinter

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sat, 04 Dec 2021 13:20:48 +0300

halide (13.0.1-2) unstable; urgency=medium

  * No changes from the previous version, but a source-only upload this time.

 -- Roman Lebedev <lebedev.ri@gmail.com>  Thu, 25 Nov 2021 11:27:50 +0300

halide (13.0.1-1) unstable; urgency=medium

  * Update to newest upstream release

 -- Roman Lebedev <lebedev.ri@gmail.com>  Mon, 22 Nov 2021 12:39:22 +0300

halide (12.0.1-1) unstable; urgency=medium

  * Initial halide package for debian (Closes: #995440)

 -- Roman Lebedev <lebedev.ri@gmail.com>  Sat, 02 Oct 2021 00:43:26 +0300
